namespace: MyFolder
flow:
  name: MyFlow
  workflow:
    - MyFlow:
        do:
          MyFolder.MyFlow: []
        navigate:
          - SUCCESS: SUCCESS
  results:
    - SUCCESS
extensions:
  graph:
    steps:
      MyFlow:
        x: 372
        y: 90
        navigate:
          e9814eae-75e6-5ba9-7a13-8162a4243d22:
            targetId: 61a7f259-4f98-61d2-e50c-cdc2eef653dd
            port: SUCCESS
    results:
      SUCCESS:
        61a7f259-4f98-61d2-e50c-cdc2eef653dd:
          x: 488
          y: 166
